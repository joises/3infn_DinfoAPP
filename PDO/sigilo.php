<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>COOKIE</title>
</head>
<body>
<?php
if(!isset($_SESSION['usuario'])){
    header("Location: login.php");
    exit();
}

echo "Seja bem vindo(a) ".
    $_SESSION["usuario"]."<br>";
echo "Seu documento é igual a ". $_SESSION['docto']."<br>";
//unset($_SESSION['docto']);


echo "<h1>Conteúdo Sigiloso</h1><br>";
echo "<h2>Obrigatório autenticação!</h2>";

?>

<br><br>
<a href="logout.php">Logout</a>

</body>
</html>