<?php
// Modifica a zona de tempo a ser utilizada. Disnovível desde o PHP 5.1
date_default_timezone_set('UTC');

$agora = time();
echo "Daqui a uma semana: ",date("d/m/Y",strtotime(" +7 day "));
$agora2 = date("d/m/Y H:i:s");
echo "<br>Agora = ",$agora2;

$dia = 22;
$mes = 11;
$ano = 2022;
$futuro = mktime(0,0,0,$mes,$dia,$ano);
echo "<br>Futuro = ",date("d/m/Y",$futuro);
$liberdade = $futuro - $agora;
echo "<br>Liberdade = ",($liberdade/(24*60*60));
echo "<br>ou Liberdade = ",($liberdade/(365*24*60*60));