<?php
require_once "conexao.php";

$acao = isset($_REQUEST['acao']) ? $_REQUEST['acao'] : null;

$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$sigla = isset($_REQUEST['sigla']) ? $_REQUEST['sigla'] : null;
$nome = isset($_REQUEST['nome']) ? $_REQUEST['nome'] : null;
$chefe = isset($_REQUEST['chefe']) ? $_REQUEST['chefe'] : null;

$erro = null;
$deptos = null;
$mensagem=array();

if ($acao == 'incluir') {
    if($sigla!=null && $nome!=null) {

        $sql = "INSERT INTO departamento ";
        $sql .= "(sigla,nome,chefe)";
        $sql .= "VALUES(?,?,?) ";

        $stmt = $cn->prepare($sql);
$stmt->bindParam(1, $sigla);
$stmt->bindParam(2, $nome);
$stmt->bindParam(3, $chefe);

if ($stmt->execute()) {
$erro = $stmt->errorCode();
$mensagem[$erro]="Departamento criado com sucesso!";
} else {
$erro = $stmt->errorCode();
$mensagem[$erro]=implode(",", $stmt->errorInfo());
}
}else{
$erro = 'Inclusao';
if($sigla==null){ $mensagem['sigla'] = "Campo SIGLA inválido!";}
if($nome==null){ $mensagem['nome'] = "Campo NOME inválido!";}
}
}elseif ($acao == 'excluir') {
if($id!=null) {
$sql = "DELETE FROM departamento ";
$sql .= "WHERE id=?";
$stmt = $cn->prepare($sql);
$stmt->bindParam(1, $id);
if ($stmt->execute()) {
$erro = $stmt->errorCode();
$mensagem['sucesso']="Departamento excluido com sucesso!";
} else {
$erro =$stmt->errorCode();
$mensagem = implode(",", $stmt->errorInfo());
}
}else{
$erro = "Exclusao";
$mensagem['id'] = "Campo ID ausente!";
}
}

//Carrega os dados do BD para atualizar a tabela HTML
$sql = "SELECT * FROM departamento ";
$rs = $cn->prepare($sql);
if($rs->execute()){
$deptos = $rs->fetchAll(PDO::FETCH_OBJ);
}


require_once "conexao.php";
$acao = isset($_REQUEST['acao']) ? $_REQUEST['acao'] : null;

$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$sigla = isset($_REQUEST['sigla']) ? $_REQUEST['sigla'] : null;
$nome = isset($_REQUEST['nome']) ? $_REQUEST['nome'] : null;
$chefe = isset($_REQUEST['chefe']) ? $_REQUEST['chefe'] : null;

$depto = null;
$erro = null;
$mensagem=array();

if($acao=='editar') {
    $sql = "SELECT * FROM departamento ";
    $sql .= "WHERE id=?";
    $rs = $cn->prepare($sql);
    $rs->bindParam(1, $id);
    if ($rs->execute()) {
        $depto = $rs->fetch(PDO::FETCH_OBJ);
        $sigla = $depto->sigla;
        $nome = $depto->nome;
        $chefe = $depto->chefe;
    }
}elseif ($acao == 'atualizar') {
    if($id==null){ $mensagem['id'] = "Campo ID ausente!";}
    if($sigla==null){ $mensagem['sigla'] = "Campo SIGLA inválido!";}
    if($nome==null){ $mensagem['nome'] = "Campo NOME inválido!";}
    exit();
    if ($id != null && $sigla != null && $nome != null) {
        $sql = "UPDATE departamento SET sigla=?,nome=?,chefe=? ";
        $sql .= "WHERE id=?";
        $stmt = $cn->prepare($sql);
        $stmt->bindParam(1, $sigla);
        $stmt->bindParam(2, $nome);
        $stmt->bindParam(3, $chefe);
        $stmt->bindParam(4, $id);
        if ($stmt->execute()) {
            $erro = "Departamento atualizado com sucesso!";
            echo $erro;
            print "<meta http-equiv='refresh' content='1;url=Departamentos.php'>";
            exit(0);
        } else {
            $erro = implode(",", $stmt->errorInfo());
        }
    } else {
        $erro = "Dados inválidos!";
    }
}

?>
<!DOCTYPE html>
<html data-wf-domain="dinfoapp.webflow.io" data-wf-page="5907683c0493323d42c83eb3" data-wf-site="5907683b0493323d42c83ea9" data-wf-status="1">
<head>
    <meta charset="utf-8">
    <title>Departamentos</title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="DinfoApp" name="generator">
    <link href="https://daks2k3a4ib2z.cloudfront.net/5907683b0493323d42c83ea9/css/dinfoapp.webflow.1a2d709b8.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js">

    </script>
    <script type="text/javascript">
        WebFont.load({
    google: {
        families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]
    }
});
    </script>
    <script src="https://daks2k3a4ib2z.cloudfront.net/0globals/modernizr-2.7.1.js" type="text/javascript">

    </script>
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
<div class="navigation-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
    <div class="w-container">
        <a class="w-nav-brand" href="Site.html">
            <div class="site-name">DinfoApp</div>
        </a>
        <nav class="navigation-menu w-nav-menu" role="navigation">
            <div class="w-dropdown" data-delay="0">
                <div class="w-dropdown-toggle">
                    <div>Lançamentos</div>
                    <div class="w-icon-dropdown-toggle">

                    </div>
                </div>
                <nav class="w-dropdown-list">
                    <a class="w-dropdown-link" href="#">Médias</a>
                    <a class="w-dropdown-link" href="#">Faltas</a>
                    <a class="w-dropdown-link" href="#">Eventos</a>
                </nav></div><a class="w-nav-link" href="login.php">Login</a>
        </nav><div class="menu-button w-nav-button">
        <div class="w-icon-nav-menu">

        </div>
    </div>
        <nav class="navigation-menu w-nav-menu" role="navigation">
            <div class="w-dropdown" data-delay="0">
                <div class="w-dropdown-toggle">
                    <div>Cadastro</div>
                    <div class="w-icon-dropdown-toggle">

                    </div>
                </div>
                <nav class="w-dropdown-list">
                    <a class="w-dropdown-link" href="Departamentos.php">Departamentos</a>
                    <a class="w-dropdown-link" href="#">Disciplina</a>
                    <a class="w-dropdown-link" href="#">Pessoas</a>
                    <a class="w-dropdown-link" href="#">Horário</a>
                </nav>
            </div>
        </nav>
        <nav class="navigation-menu w-nav-menu" role="navigation">
            <a class="w-nav-link" href="Site.html">Home</a>
        </nav>
    </div>
</div>
<div class="content-wrapper">
    <div class="w-container">
        <div class="w-row">
            <div class="w-col w-col-3 w-hidden-small w-hidden-tiny">
                <div class="white-wrapper">
                    <img class="circle-profile" src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907686025dc275d10c3b4a9_cotil_site.png">
                    <p class="site-description">Projeto DinfoApp<br><br>3INN</p>
                    <div class="grey-rule"></div>
                    <h2 class="small-heading">
                        <em>Desenvolvedores:</em>
                        <br>
                        <br>
                        <em>Josef Henrique</em>
                        <br>
                        <br>
                        <em>Vitor Toledo</em>
                    </h2>
                    <div class="grey-rule">

                    </div>
                    <div class="social-link-group">
                        <a class="social-icon-link w-inline-block" href="#">
                            <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83ec5_social-03.svg" width="25">
                        </a>
                        <a class="social-icon-link w-inline-block" href="#">
                            <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83f0c_social-07.svg" width="25">
                        </a>
                        <a class="social-icon-link w-inline-block" href="#">
                            <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83f4f_social-18.svg" width="25">
                        </a>
                        <a class="social-icon-link w-inline-block" href="#">
                            <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83ed2_social-09.svg" width="25">
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-column w-col w-col-9">
                <div class="post-wrapper">
                    <div class="post-content">
                        <div class="w-form">
                            <form data-name="Email Form" id="email-form" name="email-form">
                                <label class="field-label">
                                    <strong class="important-text-2">cadastro de departamentos</strong>
                                </label>
                            </form>
                        </div>
                    </div>
                    <div class="post-content">
                        <div class="w-form">
                            <form action="?acao=incluir" method="post">
                                Sigla:<br>
                                <input type="text"
                                       name="sigla"
                                       size="10"
                                       value="<?php echo $sigla; ?>"
                                       maxlength="50">
                                <input class="w-input" size="10" id="name-4" maxlength="50" name="sigla" placeholder="Digite a sigla do departamento:  " type="text" <?php echo $sigla; ?>>
                                <BR>
                                Nome:<br>
                                <input type="text"
                                       name="nome"
                                       size="50"
                                       value="<?php echo $nome; ?>"
                                       maxlength="50">
                                <br>

                                Chefe:<br>
                                <input type="text"
                                       name="chefe"
                                       size="50"
                                       value="<?php echo $chefe; ?>"
                                       maxlength="50"> <br>
                                <br>
                                <input class="w-button" data-wait="Please wait..." type="submit" name="btnEnviar" value="Cadastrar">
                            </form>
                            <br>

                            <table BORDER="1">
                                <tr>
                                    <th>Sigla</th>
                                    <th>Nome</th>
                                    <th>Chefe</th>
                                    <th>Ações</th>
                                </tr>
                                <?php
                                 foreach($deptos as $depto)
                                  {
                                     ?>
                                <tr>
                                    <td><?php echo $depto->sigla;?></td>
                                    <td><?php echo $depto->nome;?></td>
                                    <td><?php echo $depto->chefe;?></td>
                                    <td>
                                        <a href="editaDepto.php?acao=editar&id=<?php echo $depto->id;?>">
                                            Editar</a>
                                        <a href="Departamentos.php?acao=excluir&id=<?php echo $depto->id;?>">
                                            Excluir</a>
                                    </td>
                                </tr>

                                <?php
        }
    ?>

                            </table>

                        </div>
                    </div>
                </div>
                <div class="sidebar-on-mobile">
                    <div class="white-wrapper">
                        <img class="circle-profile" src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907686025dc275d10c3b4a9_cotil_site.png">
                        <p class="site-description">Projeto DinfoApp<br><br>3INN</p>
                        <div class="grey-rule">

                        </div>
                        <h2 class="small-heading">
                            <em>Desenvolvedores:</em>
                            <br>
                            <br>
                            <em>Josef Henrique</em>
                            <br>
                            <br>
                            Vitor Toledo</em>
                        </h2>
                        <div class="grey-rule">

                        </div>
                        <div class="social-link-group">
                            <a class="social-icon-link w-inline-block" href="#">
                                <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83ec5_social-03.svg" width="25">
                            </a>
                            <a class="social-icon-link w-inline-block" href="#">
                                <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83f0c_social-07.svg" width="25">
                            </a>
                            <a class="social-icon-link w-inline-block" href="#">
                                <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83f4f_social-18.svg" width="25">
                            </a>
                            <a class="social-icon-link w-inline-block" href="#">
                                <img src="http://uploads.webflow.com/5907683b0493323d42c83ea9/5907683c0493323d42c83ed2_social-09.svg" width="25">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript">
</script>

<script src="https://daks2k3a4ib2z.cloudfront.net/5907683b0493323d42c83ea9/js/webflow.387774b35.js" type="text/javascript">
</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js">
</script>

</body></>
