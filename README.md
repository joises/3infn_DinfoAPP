# README #

Este repositório tem por objetivo hospedar os exemplos utilizados nas aulas de Desenvolvimento de Aplicações Web, junto a turma INFN3, no COTIL.

Para cada projeto e/ou exercício, será criada uma nova pasta com nome correspondente. 
Por exemplo: 
* A primeira ativididade será criar uma aplicação para cálculo da tabuada, que estará em uma pasta chamada "tabuada".

